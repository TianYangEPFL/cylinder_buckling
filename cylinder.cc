

/* -------------------------------------------------------------------------- */
#include "non_linear_solver.hh"
#include "solid_mechanics_model.hh"
#include <iostream>
/* -------------------------------------------------------------------------- */

using namespace akantu;


/* -------------------------------------------------------------------------- */
int main(int argc, char * argv[]) {
  initialize("material.dat", argc, argv);

  const UInt spatial_dimension = 3;

  Mesh mesh(spatial_dimension);
  mesh.read("cylinder.msh");

  SolidMechanicsModel model(mesh);

  /// model initialization
  model.initFull(_analysis_method = _static);

  model.setBaseName("refined1201");
  model.addDumpFieldVector("displacement");
  model.addDumpField("external_force");
  model.addDumpField("internal_force");
  model.addDumpField("stress");
  model.addDumpField("strain");
  model.addDumpField("Green strain");
  model.addDumpField("Von Mises stress");


  //Vector<Real> surface_traction{0., 0., -1000.};
  //model.applyBC(BC::Neumann::FromTraction(surface_traction), "top");
  // Dirichlet boundary conditions
  model.applyBC(BC::Dirichlet::FixedValue(0.0, _x), "top");
  model.applyBC(BC::Dirichlet::FixedValue(0.0, _y), "top");
  model.applyBC(BC::Dirichlet::FixedValue(0.0, _z), "top");

  model.applyBC(BC::Dirichlet::FixedValue(0.0, _x), "bottom");
  model.applyBC(BC::Dirichlet::FixedValue(0.0, _y), "bottom");
  model.applyBC(BC::Dirichlet::FixedValue(0.0, _z), "bottom");
  model.dump();

  auto & solver = model.getNonLinearSolver();
  solver.set("max_iterations", 200);
  solver.set("threshold", 2e-4);
  solver.set("convergence_type", SolveConvergenceCriteria::_solution);


  int steps = 1;
  for (int ii = 1; ii <= steps; ii++) {

      model.applyBC(BC::Dirichlet::IncrementValue(-0.5, _z), "top");

      debug::setDebugLevel(dblInfo);
      model.solveStep();
      debug::setDebugLevel(dblError);

      model.dump();

      Int ite = solver.get("nb_iterations");
      Real error = solver.get("error");
      std::cout << "Steps = " << ii << " Iteration number: " << ite << " Error: " << error << std::endl;
  }

  //model.applyBC(BC::Neumann::FromTraction(surface_traction), "top");


  finalize();

  return EXIT_SUCCESS;
}
