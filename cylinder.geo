// Gmsh project created on Tue Nov 15 13:32:32 2022
SetFactory("OpenCASCADE");

eps = 1e-1;
R1 = 50;  //radius
t = 1/50 * R1; //thickness
R2 = R1 + t;
H = 200;  // height

Circle(1) = {0, 0, 0, R1}; Curve loop(1) = {1};
Circle(2) = {0, 0, 0, R2}; Curve loop(2) = {2};

Circle(3) = {0, 0, H, R1}; Curve loop(3) = {3};
Circle(4) = {0, 0, H, R2}; Curve loop(4) = {4};

ThruSections(1) = {1, 3};
ThruSections(2) = {2, 4};

BooleanDifference(3) = { Volume{2}; Delete; }{ Volume{1}; Delete; };

Rectangle(101) = {-(R2 + 1), 0, -2, 2*(R2+2), 2*(H+2)};
Rotate{{1, 0, 0}, {0, 0, -2}, Pi/2} {Surface{101};}

Rectangle(102) = {0, -(R2 + 1), -2, 2*(H+2), 2*(R2+2)};
Rotate{{0, 1, 0}, {0, 0, -2}, -Pi/2} {Surface{102};}

BooleanFragments{ Volume{3}; Delete; }{ Surface{101:102}; Delete; }
Recursive Delete { Surface{:}; }


c() = {};
c() += Curve In BoundingBox{R1 - eps, 0 - eps, 0 - eps, R2 + eps, 0 + eps, H + eps};
c() += Curve In BoundingBox{-R2 - eps, 0 - eps, 0 - eps, -R1 + eps, 0 + eps, H + eps};
c() += Curve In BoundingBox{ 0 - eps, R1 - eps, 0 - eps,  0 + eps, R2 + eps, H + eps};
c() += Curve In BoundingBox{ 0 - eps, -R2 - eps, 0 - eps,  0 + eps, -R1 + eps, H + eps};

d() = {};
d() += Curve In BoundingBox{R2 - eps, 0 - eps, 0 - eps, R2 + eps, 0 + eps, H + eps};
d() += Curve In BoundingBox{R1 - eps, 0 - eps, 0 - eps, R1 + eps, 0 + eps, H + eps};
d() += Curve In BoundingBox{-R2 - eps, 0 - eps, 0 - eps, -R2 + eps, 0 + eps, H + eps};
d() += Curve In BoundingBox{-R1 - eps, 0 - eps, 0 - eps, -R1 + eps, 0 + eps, H + eps};
d() += Curve In BoundingBox{ 0 - eps, R2 - eps, 0 - eps,  0 + eps, R2 + eps, H + eps};
d() += Curve In BoundingBox{ 0 - eps, R1 - eps, 0 - eps,  0 + eps, R1 + eps, H + eps};
d() += Curve In BoundingBox{ 0 - eps, -R2 - eps, 0 - eps, 0 + eps, -R2 + eps, H + eps};
d() += Curve In BoundingBox{ 0 - eps, -R1 - eps, 0 - eps, 0 + eps, -R1 + eps, H + eps};



Transfinite Curve{:} = 51;    // 1/4 circle
Transfinite Curve{c()} = 16;  // thickness
Transfinite Curve{d()} = 101;  // height
Transfinite Surface{:};
Transfinite Volume{:};
Recombine Surface{:};
Recombine Volume{:};

// Mesh.MeshSizeFactor = 0.3;
//+
Physical Volume("All", 33) = {4, 3, 2, 1};
//+
Physical Surface("top", 34) = {19, 13, 3, 10};
//+
Physical Surface("bottom", 35) = {8, 18, 15, 5};

Mesh 3;
Save "cylinder.msh";
